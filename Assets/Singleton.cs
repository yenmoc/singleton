﻿using UnityEngine;

// ReSharper disable once CheckNamespace
namespace UnityModule.Singleton
{
    // ReSharper disable once ClassWithVirtualMembersNeverInherited.Global
    public class Singleton<T> : MonoBehaviour where T : Component
    {
        private static T instance;

        // ReSharper disable once MemberCanBePrivate.Global
        public static T Instance
        {
            get
            {
                if (instance != null) return instance;
                instance = (T) FindObjectOfType(typeof(T));
                if (instance == null)
                {
#if UNITY_EDITOR
                    Debug.LogWarning(typeof(T) + " is not correct!");
#endif
                }

                return instance;
            }
        }

        protected virtual void Awake()
        {
            if (CheckInstance())
                DontDestroyOnLoad(gameObject);
        }

        private bool CheckInstance()
        {
            if (this == Instance)
            {
                return true;
            }

            Destroy(this);
            return false;
        }
    }
}